import os
import re

from nea import constants


def save_text(text, file_name):
    text = text.replace('\n', ' ')
    with open(constants.text + str(file_name) + '.txt', mode='a', encoding="utf-8") as f:
        f.write(text)


def save_elements(extraction_nodes, file_name):
    with open(constants.out + str(file_name) + '.txt', mode='a', encoding="utf-8") as f:
        f.write(','.join(extraction_nodes))


def read_extraction_nodes(file_path):
    with open(file_path, encoding="utf-8") as f:
        return f.read().split(',')


def read_all_extraction_nodes():
    en = {}
    for file_name in os.listdir(constants.out):
        file_path = os.path.join(constants.out, file_name)
        en[file_name] = read_extraction_nodes(file_path)
    return en


def purify_text(text):
    return text.lower().replace('\n', ' ').replace(',', ' ').replace('.', ' ')


def any_of_list_in_text(graph, node, splitted_text):
    node_set = graph.en_dictionary.dictionary[node]
    for node_name in node_set:
        if node_name in splitted_text:
            return True
    return False


def save_if_eligible(graph, text, file_name, min_elements_in_txt):
    split_text = purify_text(text).split(' ')
    elements_found_in_txt = []

    if not any_of_list_in_text(graph, graph.extracting_nodes.primary_stimulus, split_text):
        return
    elements_found_in_txt.append(graph.extracting_nodes.primary_stimulus)

    for node in graph.extracting_nodes.primary_associations:
        if any_of_list_in_text(graph, node, split_text):
            elements_found_in_txt.append(node)

    if len(elements_found_in_txt) >= min_elements_in_txt + 1:
        save_text(text, file_name)
        save_elements(elements_found_in_txt, file_name)


def clear_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)
    else:
        for file_name in os.listdir(path):
            file_path = os.path.join(path, file_name)
            os.remove(file_path)


def filter_pap(pap_file, graph, min_hits):
    clear_folder(constants.out)
    clear_folder(constants.text)
    with open(pap_file, encoding="utf-8") as file:
        text = ""
        cnt = 0
        text_name = ''
        for line in file:
            if not re.match('#\d\d\d\d\d\d', line):  # not new text
                text += line
            else:
                if len(text) > 0:  # not first line
                    save_if_eligible(graph, text, text_name, min_hits)
                    text = ""
                text_name = line.replace('\n', '')
                cnt += 1

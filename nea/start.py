import igraph

from nea import algorithm
from nea import constants
from nea.graph_loader import get_graph_from_dir
from nea.text_filter import filter_pap, read_all_extraction_nodes, clear_folder


def save_graph(g):
    visual_style = {"bbox": (2048, 2048), "margin": 20}
    lay = g.layout_fruchterman_reingold()
    visual_style["layout"] = lay
    g.vs[constants.label] = g.vs[constants.node_name]
    g.es[constants.label] = g.es[constants.edge_weight]
    igraph.plot(g, constants.graphs + g[constants.graph_name] + ".png", **visual_style)


def start():
    clear_folder(constants.graphs)
    graph = get_graph_from_dir(constants.vodka)
    g = graph.graph
    g[constants.graph_name] = "base_graph"
    save_graph(g)
    filter_pap(constants.pap + 'pap_all.txt', graph, 2)
    return graph


def do_algorithm(extraction_nodes_list, graph, param):
    base_graph = algorithm.nea_algorithm(graph, extraction_nodes_list[:3], param)

    for i in range(4, len(extraction_nodes_list) + 1):
        sub_graph = algorithm.nea_algorithm(graph, extraction_nodes_list[:i], param)
        if algorithm.at_least_half_base_vertices(base_graph, sub_graph):
            base_graph = sub_graph
        else:
            return base_graph

    return base_graph


g = start()
en = read_all_extraction_nodes()
print(en)
for k in en.keys():
    print(k)
    extraction_nodes = en[k]

    if len(extraction_nodes) > 3:
        sg = do_algorithm(extraction_nodes, g.graph, 3)
    else:
        sg = algorithm.nea_algorithm(g.graph, extraction_nodes, 3)

    sg[constants.graph_name] = k.replace(".txt", "")
    save_graph(sg)
    print("************************************************")

    # print(read_all_extraction_nodes())
import itertools

from nea import constants
from nea.graph_loader import get_vertex_index, create_empy_graph, add_edge


def at_least_half_base_vertices(base_graph, sub_graph):
    base_graph_vertices = base_graph.vs[constants.node_name]
    sub_graph_vertices = sub_graph.vs[constants.node_name]
    cnt = 0

    for v in sub_graph_vertices:
        if v in base_graph_vertices:
            cnt += 1

    return cnt >= (len(base_graph_vertices) / 2)


def add_all_to_graph(sub_graph, shortest_path, graph):
    for i in range(1, len(shortest_path)):
        v1 = graph.vs[shortest_path[i - 1]]
        v2 = graph.vs[shortest_path[i]]
        eid = graph.get_eid(v1.index, v2.index, error=False)
        w = graph.es[eid][constants.edge_weight]
        print('Adding edge from SP: ', v1['name'], '-', v2['name'], ':', w)
        add_edge(sub_graph, v1[constants.node_name], v2[constants.node_name], w)


def nea_algorithm(graph, extraction_nodes, max_nodes_in_path):
    sub_graph = create_empy_graph()
    enp = itertools.permutations(extraction_nodes, 2)
    for v1, v2 in enp:
        v1_index = get_vertex_index(graph, v1)
        v2_index = get_vertex_index(graph, v2)
        eid = graph.get_eid(v1_index, v2_index, error=False)
        if eid >= 0:
            shortest_path = graph.get_shortest_paths(v1_index, v2_index, weights=constants.edge_weight)[0]
            shortest_path_weight = graph.shortest_paths_dijkstra(v1_index, v2_index, weights=constants.edge_weight)[0][0]
            print('vertices: ' + str((v1, v2)))
            print('shortest path: ' + str(shortest_path))
            print('shortest path weight: ' + str(shortest_path_weight))
            print()
            original_path_weight = graph.es[eid][constants.edge_weight]
            add_edge(sub_graph, v1, v2, original_path_weight)

            if shortest_path_weight < original_path_weight and len(shortest_path) <= max_nodes_in_path:
                add_all_to_graph(sub_graph, shortest_path, graph)

    return sub_graph
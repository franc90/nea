import csv
from os import listdir
from os.path import isfile, join, basename, dirname

from igraph import Graph

from nea import constants
from nea.ExtractingNodes import ExtractingNodes
from nea.NodeDictionary import NodesDictionary


class NeaGraph(object):
    def __init__(self, graph=None, extracting_nodes=None, dictionary=None):
        self.graph = graph
        self.extracting_nodes = extracting_nodes
        self.en_dictionary = dictionary


def read_file(path):
    associations = []
    with open(path, encoding="utf-8") as file:
        reader = csv.reader(file, delimiter=';')
        for line in reader:
            associations.append((line[0], int(line[1])))
    return associations


def get_primary_stimulus(directory):
    return basename(dirname(directory))


def get_file_names(directory):
    return [f for f in listdir(directory) if isfile(join(directory, f))]


def add_vertex(graph, vertex_name):
    if vertex_name not in graph.vs[constants.node_name]:
        graph.add_vertex(vertex_name)


def get_vertex_index(graph, name):
    return graph.vs[constants.node_name].index(name)


def add_edge(graph, stimulus, association, weight):
    add_vertex(graph, association)
    add_vertex(graph, stimulus)

    stimulus_index = get_vertex_index(graph, stimulus)
    association_index = get_vertex_index(graph, association)

    if graph.get_eid(stimulus_index, association_index, error=False) == -1:
        graph.add_edge(stimulus_index, association_index)
        graph[stimulus_index, association_index] = weight


def add_associations(graph, stimulus, associations, total_votes):
    add_vertex(graph, stimulus)

    primary_associations = []

    for a in associations:
        add_edge(graph, stimulus, a[0], total_votes / a[1])
        primary_associations.append(a[0])

    return primary_associations


def get_stimulus_name(file):
    return file[:file.index('.')]


def get_total_votes(associations):
    cnt = 0
    for a in associations:
        cnt += a[1]
    return cnt


def create_empy_graph():
    graph = Graph(directed=False)
    graph.es[constants.edge_weight] = 1
    graph.vs[constants.node_name] = None
    return graph


def get_graph_from_dir(directory):
    graph = create_empy_graph()

    file_names = get_file_names(directory)
    primary_stimulus = get_primary_stimulus(directory)
    primary_associations = []

    for file in file_names:
        stimulus = get_stimulus_name(file)
        associations = read_file(directory + '/' + file)
        total_votes = 850

        associations_list = add_associations(graph, stimulus, associations, total_votes)
        if primary_stimulus == stimulus:
            primary_associations = associations_list

    extracting_nodes = ExtractingNodes(primary_stimulus, primary_associations)
    nodes_directory = NodesDictionary()
    nodes_directory.add(primary_stimulus)
    nodes_directory.add_all(primary_associations)
    return NeaGraph(graph, extracting_nodes, nodes_directory)

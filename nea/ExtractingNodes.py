class ExtractingNodes(object):
    def __init__(self, primary_stimulus=None, primary_associations=None):
        self.primary_stimulus = primary_stimulus
        if not primary_associations:
            self.primary_associations = []
        else:
            self.primary_associations = primary_associations
import os

from nea import constants


for file_name in os.listdir(constants.out):
    print(file_name)
    file_path = os.path.join(constants.text, file_name)
    with open(file_path, encoding="utf-8") as txt_file:
        txt = txt_file.read()
        print(txt)

    file_path = os.path.join(constants.out, file_name)
    with open(file_path, encoding="utf-8") as en_file:
        en = en_file.read().split(',')
        for i in range(0, len(en) - 1):
            for j in range(i + 1, len(en)):
                print(en[i], '  ', en[j])
    print()
from nea import constants


class NodesDictionary(object):
    def __init__(self):
        self.dictionary = {}

    def add_all(self, node_names):
        for name in node_names:
            self.add(name)

    def add(self, node_name):
        if node_name not in self.dictionary:
            self.dictionary[node_name] = set()

        # uncomment to use full dictionary
        # with open(constants.pap + 'odm.txt', encoding='utf-8') as ordBook:
        with open(constants.pap + 'odm2.txt', encoding='utf-8') as ordBook:
            for line in ordBook:
                synonyms = line.replace(' ', '').replace('\n', '').split(',')
                if node_name in synonyms:
                    self.add_to_dictionary(node_name, synonyms)
                    # uncomment to create simplified dict for faster checking
                    # with open(constants.pap + 'odm2.txt', encoding='utf-8', mode='a') as newOdm:
                    #     newOdm.write(line)

    def add_to_dictionary(self, node_name, synonyms):
        node_set = self.dictionary[node_name]
        for synonym in synonyms:
            node_set.add(synonym.strip())
